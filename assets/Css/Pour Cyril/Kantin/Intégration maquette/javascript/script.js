$(document).ready(function () {

    /* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ */
    /* Navbar Color */
    $('#contact-nav').css('color', '#009970')

    $('#home-start').on({
        mouseenter: function () {
            $('#home-nav').css('color', '#009970')
        },
        mouseleave: function () {
            $('#home-nav').css('color', '')
        }
    });
    $('#about-start').on({
        mouseenter: function () {
            $('#about-nav').css('color', '#009970')
        },
        mouseleave: function () {
            $('#about-nav').css('color', '')
        }
    });
    $('#services-start').on({
        mouseenter: function () {
            $('#services-nav').css('color', '#009970')
        },
        mouseleave: function () {
            $('#services-nav').css('color', '')
        }
    });
    $('#portfolio-start').on({
        mouseenter: function () {
            $('#portfolio-nav').css('color', '#009970')
        },
        mouseleave: function () {
            $('#portfolio-nav').css('color', '')
        }
    });
    $('#team-start').on({
        mouseenter: function () {
            $('#team-nav').css('color', '#009970')
        },
        mouseleave: function () {
            $('#team-nav').css('color', '')
        }
    });


    /* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ */
    /* Header changing color according to the device width */
    // Change color of the header in small and medium device
    function headerColor () {
        // Small/Medium device
        if (window.matchMedia('(max-width: 992px)').matches) {
            $('.bg-color').addClass('bg-success').removeClass('bg-light')
            $('.nav-link').addClass('text-white ms-3')
        }
    }

    headerColor()


    /* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ */
    /* Button Back To Top */
    $('#btn-scroll-top').hide();
    $(window).scroll(function () {
        let height = $(window).scrollTop();
    
        if (height < 300) {
            $('#btn-scroll-top').hide()
        }
        if(height  > 300) {
            $('#btn-scroll-top').show()
        }
    });


    /* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ */
    /* Client On Hover Show Color */
    // Set all the image to be black
    $('.client-img').css('filter', 'brightness(0)')

    // Client 1 Image hover effect
    $(".client-1").on({
        // Set the brightness to 1 so the color of the image are show when the mouse is above
        mouseenter: function () {
            $(this).css('filter', 'brightness(1)')
        },
        // Set the brightness to "default" so the image is black when the mouse leave the hover
        mouseleave: function () {
            $(this).css('filter', 'brightness(0)')
        }
    });

    // Client 2 Image hover effect
    $(".client-2").on({
        mouseenter: function () {
            $(this).css('filter', 'brightness(1)')
        },
        mouseleave: function () {
            $(this).css('filter', 'brightness(0)')
        }
    });

    // Client 3 Image hover effect
    $(".client-3").on({
        mouseenter: function () {
            $(this).css('filter', 'brightness(1)')
        },
        mouseleave: function () {
            $(this).css('filter', 'brightness(0)')
        }
    });

    // Client 4 Image hover effect
    $(".client-4").on({
        mouseenter: function () {
            $(this).css('filter', 'brightness(1)')
        },
        mouseleave: function () {
            $(this).css('filter', 'brightness(0)')
        }
    });

    // Client 5 Image hover effect
    $(".client-5").on({
        mouseenter: function () {
            $(this).css('filter', 'brightness(1)')
        },
        mouseleave: function () {
            $(this).css('filter', 'brightness(0)')
        }
    });

    // Client 6 Image hover effect
    $(".client-6").on({
        mouseenter: function () {
            $(this).css('filter', 'brightness(1)')
        },
        mouseleave: function () {
            $(this).css('filter', 'brightness(0)')
        }
    });


    /* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ */
    /* OWL Carousel JQuery */
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },
            600:{
                items:1,
            },
            1000:{
                items:2,
                dotsEach:1,
            }
        }
    })


    /* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ */
    /* Isotope JQuery */
    // init Isotope
    let $grid = $('.grid').isotope({
    // options
        itemSelector: '.grid-item',
    });
    // filter items on button click
    $('.filter-button-group').on( 'click', 'button', function() {
        let filterValue = $(this).attr('data-filter');
        $grid.isotope({ filter: filterValue });
    });


    /* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ */
    /* Change color of the filter button on click for the portfolio */

    $('.btn-filter').click(function () {
        console.log('1');
    })


    $('#btn-portfolio-all').on("click", function () {
        // $('.btn-filter').addClass('btn-secondary').removeClass('btn-outline-secondary');
        $('#btn-portfolio-all').addClass('btn-secondary').removeClass('btn-outline-secondary');
        $('#btn-portfolio-app').addClass('btn-outline-secondary').removeClass('btn-secondary');
        $('#btn-portfolio-card').addClass('btn-outline-secondary').removeClass('btn-secondary');
        $('#btn-portfolio-web').addClass('btn-outline-secondary').removeClass('btn-secondary');
    })

    $('#btn-portfolio-app').on("click", function () {
        $('#btn-portfolio-app').addClass('btn-secondary').removeClass('btn-outline-secondary');
        $('#btn-portfolio-all').addClass('btn-outline-secondary').removeClass('btn-secondary');
        $('#btn-portfolio-card').addClass('btn-outline-secondary').removeClass('btn-secondary');
        $('#btn-portfolio-web').addClass('btn-outline-secondary').removeClass('btn-secondary');
    })

    $('#btn-portfolio-card').on("click", function () {
        $('#btn-portfolio-card').addClass('btn-secondary').removeClass('btn-outline-secondary');
        $('#btn-portfolio-all').addClass('btn-outline-secondary').removeClass('btn-secondary');
        $('#btn-portfolio-app').addClass('btn-outline-secondary').removeClass('btn-secondary');
        $('#btn-portfolio-web').addClass('btn-outline-secondary').removeClass('btn-secondary');
    })

    $('#btn-portfolio-web').on("click", function () {
        $('#btn-portfolio-web').addClass('btn-secondary').removeClass('btn-outline-secondary');
        $('#btn-portfolio-all').addClass('btn-outline-secondary').removeClass('btn-secondary');
        $('#btn-portfolio-app').addClass('btn-outline-secondary').removeClass('btn-secondary');
        $('#btn-portfolio-card').addClass('btn-outline-secondary').removeClass('btn-secondary');
    })


    /* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ */
    /* Increment Animation */
    let speed1 = 27;
    let speed2 = 10;
    let speed3 = 1;
    let speed4 = 500;

    /* Call this function with a string containing the ID name to
    * the element containing the number you want to do a count animation on.*/
    function incEltNbr1(id) {
        element = document.getElementById(id);
        endNbr = Number(document.getElementById(id).innerHTML);
        incNbrRec1(0, endNbr, element);
    }

    /*A recursive function to increase the number.*/
    function incNbrRec1(i, endNbr, element) {
        if (i <= endNbr) {
            element.innerHTML = i;
            setTimeout(function() {//Delay a bit before calling the function again.
            incNbrRec1(i + 1, endNbr, element);
            }, speed1);
        }
    }

    // Number 2
    function incEltNbr2(id) {
        element = document.getElementById(id);
        endNbr = Number(document.getElementById(id).innerHTML);
        incNbrRec2(0, endNbr, element);
    }

    /*A recursive function to increase the number.*/
    function incNbrRec2(i, endNbr, element) {
        if (i <= endNbr) {
            element.innerHTML = i;
            setTimeout(function() {//Delay a bit before calling the function again.
            incNbrRec2(i + 1, endNbr, element);
            }, speed2);
        }
    }

    // Number 3
    function incEltNbr3(id) {
        element = document.getElementById(id);
        endNbr = Number(document.getElementById(id).innerHTML);
        incNbrRec3(0, endNbr, element);
    }

    /*A recursive function to increase the number.*/
    function incNbrRec3(i, endNbr, element) {
        if (i <= endNbr) {
            element.innerHTML = i;
            setTimeout(function() {//Delay a bit before calling the function again.
            incNbrRec3(i + 1, endNbr, element);
            }, speed3);
        }
    }

    // Number 4
    function incEltNbr4(id) {
        element = document.getElementById(id);
        endNbr = Number(document.getElementById(id).innerHTML);
        incNbrRec4(0, endNbr, element);
    }

    /*A recursive function to increase the number.*/
    function incNbrRec4(i, endNbr, element) {
        if (i <= endNbr) {
            element.innerHTML = i;
            setTimeout(function() {//Delay a bit before calling the function again.
            incNbrRec4(i + 1, endNbr, element);
            }, speed4);
        }
    }

    incEltNbr1("number1"); /*Call this funtion with the ID-name for that element to increase the number within*/
    incEltNbr2("number2");
    incEltNbr3("number3");
    incEltNbr4("number4");

});