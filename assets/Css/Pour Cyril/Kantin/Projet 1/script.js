$(document).ready(function () {

    // _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
    // Responsive footer
    function CheckResponsive () {
        // Header responsive
        if (window.matchMedia('(max-width: 1407px)').matches) {
            $('.menu-size').addClass('fs-4').removeClass('fs-3')
        }
        if (window.matchMedia('(max-width: 1242px)').matches) {
            $('.menu-size').addClass('fs-5').removeClass('fs-4')
            $('.separator').addClass('fs-6').removeClass('fs-5')
            $('.menu-text').addClass('me-1').removeClass('me-2')
        }
        if (window.matchMedia('(max-width: 1077px)').matches) {
            $('.menu-size').addClass('fs-6').removeClass('fs-5')
        }
        if (window.matchMedia('(max-width: 852px)').matches) {
            
        }
        // Footer responsive
        if (window.matchMedia('(max-width: 680px)').matches) {
            $('.footer-div').addClass('flex-column').removeClass('flex-row');
            $('.responsive-footer-menu').css('order', '1').addClass('fs-6').removeClass('fs-5')
            $('.responsive-footer-copyright').css('order', '3');
            $('.responsive-footer-form').css('order', '2').addClass('mb-3')
        }

    }

    CheckResponsive ()


    // _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
    // Scroll to top button that show when scroll underneath the header
    $('#btn-scroll').hide();
    $(window).scroll(function () {
        let height = $(window).scrollTop();
    
        if (height < 155) {
            $('#btn-scroll').hide()
        }
        if(height  > 155) {
            $('#btn-scroll').show()
        }
    });


    // _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
    // Change header and footer color when clicking on the color palette button then on clicking on one of the colored circle
    $('#color-change').hide();
    let presentColor = 'default';

    // Function that hide the button of the present color in the header and the footer and show the other color that we can choose from
    function PresentColor () {
        if (presentColor == 'default') {
            $('#color-default').hide()
            $('#color-blue').show()
            $('#color-red').show()
            $('#color-green').show()
        }
        if ( presentColor == 'blue' ) {
            $('#color-blue').hide()
            $('#color-default').show()
            $('#color-red').show()
            $('#color-green').show()
        }
        if ( presentColor == 'red' ) {
            $('#color-red').hide()
            $('#color-blue').show()
            $('#color-default').show()
            $('#color-green').show()
        }
        if ( presentColor == 'green' ) {
            $('#color-green').hide()
            $('#color-blue').show()
            $('#color-red').show()
            $('#color-default').show()
        }
    }

    $('#color-palette').on("click", function () {
        $('#color-change').toggle()

        PresentColor()

        // Close change color window on x mark click
        $('#color-close').on("click", function () {
            $('#color-change').hide()
        })

        // Blue color change
        $('#color-blue').on("click", function () {
            $('.header-color').addClass('bg-primary').removeClass('bg-dark bg-danger bg-success')
            $('.footer-color').addClass('bg-primary').removeClass('bg-dark bg-danger bg-success')
            presentColor = 'blue'
            PresentColor()
        })

        // Red color change
        $('#color-red').on("click", function () {
            $('.header-color').addClass('bg-danger').removeClass('bg-dark bg-primary bg-success')
            $('.footer-color').addClass('bg-danger').removeClass('bg-dark bg-primary bg-success')
            presentColor = 'red'
            PresentColor()
        })

        // Green color change
        $('#color-green').on("click", function () {
            $('.header-color').addClass('bg-success').removeClass('bg-dark bg-danger bg-primary')
            $('.footer-color').addClass('bg-success').removeClass('bg-dark bg-danger bg-primary')
            presentColor = 'green'
            PresentColor()
        })
        
        // Back to default color
        $('#color-default').on("click", function () {
            $('.header-color').addClass('bg-dark').removeClass('bg-primary bg-danger bg-success')
            $('.footer-color').addClass('bg-dark').removeClass('bg-primary bg-danger bg-success')
            presentColor = 'default'
            PresentColor()
        })
    })


    // _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
    // Button "add a card" that a card on click and manages the colonization of the card ( by using bootstrap col system )
    let cardNumber = 3;
    let card = '<div class="col-md-6 col-lg-4 align-self-center card-added"> <div class="card card-changes mb-3"> <div class="card-body text-center fs-2"><i class="fa-solid fa-umbrella-beach"></i><h5 class="card-title"> Beach </h5><p class="card-text fs-5 fst-italic">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, ratione esse. Unde minima vero dolor libero alias, quae exercitationem, sint error odit autem obcaecati sed dignissimos praesentium? Saepe, aperiam rem.</p><a href="#" class="btn btn-secondary mb-2"> Go to beach ! </a></div></div></div>'

    function CardNumber () {
        if ( (cardNumber % 3) == 0 ) {
            $('.card-div').append(card);
            $('.card-added').addClass('col-lg-4').removeClass('col-lg-3');
        } else {
            $('.card-div').append(card);
            $('.card-added').addClass('col-lg-3').removeClass('col-lg-4');
        }
    }

    $('#add-card').on("click", function () {
        cardNumber = cardNumber + 1;
        CardNumber()
    });


    // _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
    // Animation of the text and image that slide in on scroll
    $('.image1-animation').hide()
    $('.texte1-animation').hide()
    $('.image2-animation').hide()
    $('.texte2-animation').hide()

    $(window).scroll(function () {
        let heightAnimation = $(window).scrollTop();
    
        if ( heightAnimation > 1600 ) {
            $('.texte1-animation').show(1000)
            $('.image1-animation').show(1000)
        }
        if(heightAnimation  > 2100 ) {
            $('.texte2-animation').show(1000)
            $('.image2-animation').show(1000)
        }
    });



    $('.hamburger').on('click', function () {
        if ($('.hamburger').hasClass('active')) {
            $('.hamburger').removeClass('active')
            $('.nav-menu').removeClass('active')
        }
    })
    $('.hamburger').on('click', function () {
        $('.hamburger').addClass('active')
        $('.nav-menu').addClass('active')
    })
    $('.nav-item').on('click', function () {
        $('.hamburger').removeClass('active')
        $('.nav-menu').removeClass('active')
    })






});
